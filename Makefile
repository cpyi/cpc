all: cpc cpm
CXX = g++
LINKFLAGS =
cpc: main.o reader.o scanner.o parser.o analyser.o compiler_toy.o compiler_51.o
	$(CXX) $(LINKFLAGS) main.o reader.o scanner.o parser.o analyser.o compiler_toy.o compiler_51.o -o cpc
main.o: main.cpp
	$(CXX) $(LINKFLAGS) main.cpp -c
reader.o: reader.h reader.cpp
	$(CXX) $(LINKFLAGS) reader.cpp -c
scanner.o: scanner.h scanner.cpp
	$(CXX) $(LINKFLAGS) scanner.cpp -c
parser.o: parser.h parser.cpp
	$(CXX) $(LINKFLAGS) parser.cpp -c
analyser.o: analyser.h analyser.cpp
	$(CXX) $(LINKFLAGS) analyser.cpp -c
compiler_toy.o: compiler_toy.cpp compiler_toy.h
	$(CXX) $(LINKFLAGS) compiler_toy.cpp -c
compiler_51.o: compiler_51.cpp compiler_51.h
	$(CXX) $(LINKFLAGS) compiler_51.cpp -c
cpm: machine.o driver.o
	$(CXX) $(LINKFLAGS) machine.o driver.o -o cpm
machine.o: machine.h machine.cpp
	$(CXX) $(LINKFLAGS) machine.cpp -c
driver.o: driver.cpp
	$(CXX) $(LINKFLAGS) driver.cpp -c
clean:
	rm -rf *.o *.gch cpc cpm
