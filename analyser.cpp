#include "analyser.h"
void AnalyserError::dump()
{
    std::cout << "Sementic Error: " << line+1 << ": " << mesg << std::endl;
}
void Analyser::analyzeBlockNode(BlockNode *bnodep)
{
    int i;
    ExpressionNode *enodep;
    for(i = 0;i < bnodep->getExpressionNum();++i)
    {
        enodep = bnodep->getExpression(i);
        analyzeExpressionNode(enodep);
    }
}
void Analyser::analyzeExpressionNode(ExpressionNode *enodep)
{
    switch(enodep->getNodeType())
    {
        case(VAR_NODE):
            analyzeVarNode(dynamic_cast<VarNode *>(enodep));
        break;
        case(COMPOUND_NODE):
            analyzeCompoundNode(dynamic_cast<CompoundNode *>(enodep));
        break;
    }
}
void Analyser::analyzeVarNode(VarNode *vnodep)
{
    std::map<std::string,VarNode*>::iterator it;
    it = vars.find(vnodep->getVarName());
    if(it == vars.end())
    {
        vars.insert(std::pair<std::string,VarNode*>(vnodep->getVarName(),vnodep));
    }
    else
    {
        std::string mesg = "Variable \"" + vnodep->getVarName() + "\" declared already";
        errors.push_back(AnalyserError(vnodep->getLine(),mesg));
    }
}
Analyser::VarType Analyser::analyzeCompoundNode(CompoundNode *cnodep)
{
    int i;
    CompoundNode *ccnodep;
    VarType vt = TYPE_UNSET;
    bool isCompare = false;
    std::map<std::string,VarNode*>::iterator it;
    std::string mesg;
    for(i = 0;i < cnodep->getSize();++i)
    {
        ccnodep = cnodep->getDesc(i);
        switch(ccnodep->getType())
        {
            case CompoundNode::OPERAND_INT:
                if(vt == TYPE_BOOL)
                {
                    errors.push_back(AnalyserError(cnodep->getLine(),"Invalid variable type conversion"));
                }
                vt = TYPE_INT;
            break;
            case CompoundNode::OPERAND_BOOL:
                if(vt == TYPE_INT)
                {
                    errors.push_back(AnalyserError(cnodep->getLine(),"Invalid variable type conversion"));
                }
                vt = TYPE_BOOL;
            break;
            case CompoundNode::OPERAND_IDENTIFIER:
                it = vars.find(ccnodep->getVal());
                if(it == vars.end())
                {
                    errors.push_back(AnalyserError(cnodep->getLine(),"Variable not declared"));
                }
                else
                {
                    if(it->second->getVarType() == "bool")
                    {
                        if(vt == TYPE_INT)
                        {
                            errors.push_back(AnalyserError(cnodep->getLine(),"Invalid variable type conversion"));
                        }
                        vt = TYPE_BOOL;
                    }
                    else if(it->second->getVarType() == "int")
                    {
                        if(vt == TYPE_BOOL)
                        {
                            errors.push_back(AnalyserError(cnodep->getLine(),"Invalid variable type conversion"));
                        }
                        vt = TYPE_INT;
                    }
                }
            break;
            case CompoundNode::OPERAND_MINUS:
            break;
            case CompoundNode::OPERATOR_PLUS:
                if(vt == TYPE_BOOL)
                {
                    errors.push_back(AnalyserError(cnodep->getLine(),"Operator `+` misused"));
                }
                vt = TYPE_INT;
            break;
            case CompoundNode::OPERATOR_MINUS:
                if(vt == TYPE_BOOL)
                {
                    errors.push_back(AnalyserError(cnodep->getLine(),"Operator `-` misused"));
                }
                vt = TYPE_INT;
            break;
            case CompoundNode::OPERATOR_MULT:
                if(vt == TYPE_BOOL)
                {
                    errors.push_back(AnalyserError(cnodep->getLine(),"Operator `*` misused"));
                }
                vt = TYPE_INT;
            break;
            case CompoundNode::OPERATOR_DIV:
                if(vt == TYPE_BOOL)
                {
                    errors.push_back(AnalyserError(cnodep->getLine(),"Operator `/` misused"));
                }
                vt = TYPE_INT;
            break;
            case CompoundNode::OPERATOR_GREATER:
                if(vt == TYPE_BOOL)
                {
                    errors.push_back(AnalyserError(cnodep->getLine(),"Operator `>` misused"));
                }
                vt = TYPE_INT;
                isCompare = true;
            break;
            case CompoundNode::OPERATOR_GREATEREQUAL:
                if(vt == TYPE_BOOL)
                {
                    errors.push_back(AnalyserError(cnodep->getLine(),"Operator `>=` misused"));
                }
                vt = TYPE_INT;
                isCompare = true;
            break;
            case CompoundNode::OPERATOR_LESS:
                if(vt == TYPE_BOOL)
                {
                    errors.push_back(AnalyserError(cnodep->getLine(),"Operator `<` misused"));
                }
                vt = TYPE_INT;
                isCompare = true;
            break;
            case CompoundNode::OPERATOR_LESSEQUAL:
                if(vt == TYPE_BOOL)
                {
                    errors.push_back(AnalyserError(cnodep->getLine(),"Operator `<=` misused"));
                }
                vt = TYPE_INT;
                isCompare = true;
            break;
            case CompoundNode::OPERATOR_EQUAL:
                if(vt == TYPE_BOOL)
                {
                    errors.push_back(AnalyserError(cnodep->getLine(),"Operator `==` misused"));
                }
                vt = TYPE_INT;
                isCompare = true;
            break;
            case CompoundNode::COMPOUND:
                if(analyzeCompoundNode(ccnodep) == TYPE_INT)
                {
                    if(vt == TYPE_BOOL)
                    {
                        errors.push_back(AnalyserError(cnodep->getLine(),"Invalid type conversion"));
                    }
                    vt = TYPE_INT;
                }
                else if(analyzeCompoundNode(ccnodep) == TYPE_BOOL)
                {
                    if(vt == TYPE_INT)
                    {
                        errors.push_back(AnalyserError(cnodep->getLine(),"Invalid type conversion"));
                    }
                    vt = TYPE_BOOL;
                }
            break;
        }
    }
    if(isCompare)
    {
        return TYPE_BOOL;
    }
    else
    {
        return TYPE_INT;
    }
}
void Analyser::dumpError()
{
    int i;
    for(i = 0;i < errors.size();++i)
    {
        errors[i].dump();
    }
}
