#include <map>
#include <string>
#include <iostream>
#include <vector>
#include "parser.h"
#ifndef _ANALYSER_H_
#define _ANALYSER_H_
class AnalyserError
{
    public:
        AnalyserError(int line,std::string mesg):line(line),mesg(mesg){}
        void dump();
    private:
        int line;
        std::string mesg;
};
class Analyser
{
    enum VarType
    {
        TYPE_UNSET,TYPE_INT,TYPE_BOOL
    };
    public:
        void analyzeBlockNode(BlockNode *bnodep);
        void analyzeExpressionNode(ExpressionNode *enodep);
        void analyzeVarNode(VarNode *vnodep);
        VarType analyzeCompoundNode(CompoundNode *vnodep);
        void dumpError();
    private:
        std::map<std::string,VarNode*> vars;
        std::vector<AnalyserError> errors;
};
#endif
