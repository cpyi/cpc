#include "compiler_51.h"
std::string Compiler51::getMachineCode(BlockNode *bnodep)
{
    initMachine();
    compileBlockNode(bnodep);
    return code;
}
std::string Compiler51::getNextRegister()
{
    std::stringstream ss;
    std::string regname;
    ss << ++regcnt;
    ss >> regname;
    return regname;
}
std::string Compiler51::getNextLabel()
{
    std::stringstream ss;
    std::string lblname;
    ss << "LBL" << lblcnt++;
    ss >> lblname;
    return lblname;
}
void Compiler51::writeLine(std::string line)
{
    code += line;
    code += '\n';
}
void Compiler51::compileBlockNode(BlockNode *bnodep)
{
    int i;
    ExpressionNode *enodep;
    for(i = 0;i < bnodep->getExpressionNum();++i)
    {
        enodep = bnodep->getExpression(i);
        compileExpressionNode(enodep);
    }
}
void Compiler51::compileExpressionNode(ExpressionNode *enodep)
{
    switch(enodep->getNodeType())
    {
        case(VAR_NODE):
            compileVarNode(dynamic_cast<VarNode *>(enodep));
        break;
        case(PRINT_NODE):
            compilePrintNode(dynamic_cast<PrintNode *>(enodep));
        break;
        case(COMPOUND_NODE):
            compileCompoundNode(dynamic_cast<CompoundNode *>(enodep));
            writeLine("\tPOP A");
        break;
        case(IF_NODE):
            compileIfNode(dynamic_cast<IfNode *>(enodep));
        break;
        case(WHILE_NODE):
            //compileWhileNode(dynamic_cast<WhileNode *>(enodep));
        break;
    }
}
void Compiler51::compileIfNode(IfNode *inodep)
{
    CompoundNode *condition = inodep->getCondition()->getExpression();
    BlockNode *if_block = inodep->getIfBlock();
    BlockNode *else_block = inodep->getElseBlock();
    std::string else_lbl,end_lbl;
    else_lbl = getNextLabel();
    end_lbl = getNextLabel();
    compileCompoundNode(condition);
    writeLine("\tPOP A");
    writeLine("\tJZ " + else_lbl);
    compileBlockNode(if_block);
    writeLine("\tSJMP " + end_lbl);
    writeLine(else_lbl + ":\t");
    if(else_block != NULL)
    {
        compileBlockNode(else_block);
    }
    writeLine(end_lbl + ":\t");
}
void Compiler51::compileWhileNode(WhileNode *wnodep)
{
    /*
    CompoundNode *condition = wnodep->getCondition()->getExpression();
    BlockNode *while_block = wnodep->getWhileBlock();
    std::string loop_label,end_label,false_reg;
    false_reg = getNextRegister();
    loop_label = getNextLabel();
    end_label = getNextLabel();
    writeLine("lwi " + false_reg + " 0");
    writeLine("vlabel " + loop_label);
    writeLine("beq " + compileCompoundNode(condition) + " " + false_reg + " _" + end_label);
    compileBlockNode(while_block);
    writeLine("jump _" + loop_label);
    writeLine("vlabel " + end_label);
    */
}
void Compiler51::compileVarNode(VarNode *vnodep)
{
    std::string var_reg;
    CompoundNode *cnodep;
    var_reg = getNextRegister();
    vars.insert(std::pair<std::string,std::string>(vnodep->getVarName(),var_reg));
    cnodep = vnodep->getInitExpression();
    if(cnodep != NULL)
    {
        compileCompoundNode(cnodep);
        //writeLine("\tPOP A");
        //writeLine("\tPUSH A");
    }
    else
    {
        writeLine("\tMOV A, #00H");
        writeLine("\tPUSH A");
    }
}
void Compiler51::compilePrintNode(PrintNode *pnodep)
{
    compileCompoundNode(pnodep->getExpression());
    writeLine("\tPOP A");
    writeLine("\tMOV SBUF, A");
}
void Compiler51::compileCompoundNode(CompoundNode *cnodep)
{
    int i;
    CompoundNode *desc;
    CompoundNode *oper = NULL;
    CompoundNode *current;
    bool init = 1;
    if(cnodep->getType() == CompoundNode::COMPOUND)
    {
        for(i = 0;i < cnodep->getSize();++i)
        {
            desc = cnodep->getDesc(i);
            switch(desc->getType())
            {
                case CompoundNode::OPERATOR_PLUS:
                case CompoundNode::OPERATOR_MINUS:
                case CompoundNode::OPERATOR_MULT:
                case CompoundNode::OPERATOR_DIV:
                case CompoundNode::OPERATOR_GREATER:
                case CompoundNode::OPERATOR_GREATEREQUAL:
                case CompoundNode::OPERATOR_LESS:
                case CompoundNode::OPERATOR_LESSEQUAL:
                case CompoundNode::OPERATOR_EQUAL:
                case CompoundNode::OPERATOR_ASSIGN:
                    oper = desc;
                break;
                default:
                    if(init)
                    {
                        compileCompoundNode(desc);
                        writeLine("\tPOP A");
                        init = false;
                    }
                    else
                    {
                        writeLine("\tPUSH A");
                        compileCompoundNode(desc);
                        writeLine("\tPOP B");
                        writeLine("\tPOP A");
                        if(oper->getType() == CompoundNode::OPERATOR_PLUS)
                        {
                            writeLine("\tADD A, B");
                        }
                        else if(oper->getType() == CompoundNode::OPERATOR_MINUS)
                        {
                            writeLine("\tSUBB A, B");
                        }
                        else if(oper->getType() == CompoundNode::OPERATOR_MULT)
                        {
                            writeLine("\tMUL AB");
                        }
                        else if(oper->getType() == CompoundNode::OPERATOR_DIV)
                        {
                            writeLine("\tDIV AB");
                        }
                        else if(oper->getType() == CompoundNode::OPERATOR_EQUAL)
                        {
                            std::string neq_lbl;
                            neq_lbl = getNextLabel();
                             
                            writeLine("\tMOV R0,#00H");
                            writeLine("\tCJNE A, B, " + neq_lbl);
                            writeLine("\tMOV R0, #01H");
                            writeLine(neq_lbl + ":\tMOV A, R0");
                        }
                        else if(oper->getType() == CompoundNode::OPERATOR_GREATER)
                        {
                            std::string neq_lbl,end_lbl;
                            neq_lbl = getNextLabel();
                            end_lbl = getNextLabel();
                            writeLine("\tMOV R0, #00H");
                            writeLine("\tCJNE A, B, " + neq_lbl);
                            writeLine("\tSJMP " + end_lbl);
                            writeLine(neq_lbl + ":\t" + "SUBB A, B");
                            writeLine("\tJC " + end_lbl);
                            writeLine("\tMOV R0, #01H");
                            writeLine(end_lbl + ":\tMOV A, R0");
                        }
                        else if(oper->getType() == CompoundNode::OPERATOR_GREATEREQUAL)
                        {
                            std::string less_lbl;
                            less_lbl = getNextLabel();
                            writeLine("\tSUBB A, B");
                            writeLine("\tMOV R0, #00H");
                            writeLine("\tJC " + less_lbl);
                            writeLine("\tMOV R0, #01H");
                            writeLine(less_lbl + ":\tMOV A, R0");
                        }
                        else if(oper->getType() == CompoundNode::OPERATOR_LESS)
                        {
                            std::string less_lbl;
                            less_lbl = getNextLabel();
                            writeLine("\tSUBB A, B");
                            writeLine("\tMOV R0, #00H");
                            writeLine("\tJNC " + less_lbl);
                            writeLine("\tMOV R0, #01H");
                            writeLine(less_lbl + ":\tMOV A, R0");
                        }
                        else if(oper->getType() == CompoundNode::OPERATOR_LESSEQUAL)
                        {
                            std::string neq_lbl,end_lbl;
                            neq_lbl = getNextLabel();
                            end_lbl = getNextLabel();
                            writeLine("\tMOV R0, #01H");
                            writeLine("\tCJNE A, B, " + neq_lbl);
                            writeLine("\tSJMP " + end_lbl);
                            writeLine(neq_lbl + ":\t" + "SUBB A, B");
                            writeLine("\tJC " + end_lbl);
                            writeLine("\tMOV R0, #00H");
                            writeLine(end_lbl + ":\tMOV A, R0");
                        }
                        else if(oper->getType() == CompoundNode::OPERATOR_ASSIGN)
                        {
                            //NIY
                        }
                    }
                break;
            }
        }
    }
    else if(cnodep->getType() == CompoundNode::OPERAND_INT)
    {
        if(cnodep->getVal()[0] == '-')
        {
            writeLine("\tMOV A, #" + negDecStrToByteDecStr(cnodep->getVal()));
        }
        else
        {
            writeLine("\tMOV A, #" + cnodep->getVal());
        }
    }
    else if(cnodep->getType() == CompoundNode::OPERAND_BOOL)
    {
        if(cnodep->getVal() == "true")
        {
            writeLine("\tMOV A, #1");
        }
        else
        {
            writeLine("\tMOV A, #0");
        }
    }
    else if(cnodep->getType() == CompoundNode::OPERAND_IDENTIFIER)
    {
        std::map<std::string,std::string>::iterator iter;
        iter = vars.find(cnodep->getVal());
        if(iter != vars.end())
        {
            writeLine("\tMOV R0, #47+" + iter->second);
            writeLine("\tMOV A, @R0");
        }
    }
    writeLine("\tPUSH A");
}
std::string Compiler51::negDecStrToByteDecStr(std::string str)
{
    std::stringstream ss;
    int num,bits[8],i;
    ss << str;
    ss >> num;
    num = num*-1;
    for(i = 0;i < 8;++i)
    {
        bits[i] = (num >> i)%2;
    }
    for(i = 0;i < 8;++i)
    {
        if(bits[i] == 1)
        {
            bits[i] = 0;
        }
        else if(bits[i] == 0)
        {
            bits[i] = 1;
        }
    }
    num = 0;
    for(i = 0;i < 8;++i)
    {
        num += bits[i] << i;
    }
    ++num;
    ss.clear();
    ss << num;
    ss >> str;
    return str;
}
void Compiler51::initMachine()
{
    writeLine("INIT:\t");
    writeLine("\tMOV SP, #2FH");
}
