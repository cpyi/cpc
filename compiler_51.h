#include <string>
#include <map>
#include <sstream>
#include "parser.h"
#ifndef _COMPILER51_H_
#define _COMPILER51_H_
class Compiler51
{
    public:
        Compiler51():regcnt(0),lblcnt(0),code(""){}
        std::string getMachineCode(BlockNode *bnodep);
        std::string getNextRegister();
        std::string getNextLabel();
        void writeLine(std::string line);

        //Machine Related Initialization
        void initMachine();

        //Compile Functions
        void compileBlockNode(BlockNode *bnodep);
        void compileExpressionNode(ExpressionNode *enodep);
        void compilePrintNode(PrintNode *pnodep);
        void compileVarNode(VarNode *vnodep);
        void compileIfNode(IfNode *inodep);
        void compileWhileNode(WhileNode *wnodep);
        void compileCompoundNode(CompoundNode *cnodep);

        //Utility
        std::string negDecStrToByteDecStr(std::string);
    private:
        std::string code;
        int regcnt,lblcnt;
        std::map<std::string,std::string> vars;
};
#endif
