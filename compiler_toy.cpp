#include "compiler_toy.h"
std::string CompilerToy::getMachineCode(BlockNode *bnodep)
{
    compileBlockNode(bnodep);
    return code;
}
std::string CompilerToy::getNextRegister()
{
    std::stringstream ss;
    std::string regname;
    ss << "$" << regcnt++;
    ss >> regname;
    return regname;
}
std::string CompilerToy::getNextLabel()
{
    std::stringstream ss;
    std::string lblname;
    ss << "#" << regcnt++;
    ss >> lblname;
    return lblname;
}
void CompilerToy::writeLine(std::string line)
{
    code += line;
    code += '\n';
}
void CompilerToy::compileBlockNode(BlockNode *bnodep)
{
    int i;
    ExpressionNode *enodep;
    for(i = 0;i < bnodep->getExpressionNum();++i)
    {
        enodep = bnodep->getExpression(i);
        compileExpressionNode(enodep);
    }
}
void CompilerToy::compileExpressionNode(ExpressionNode *enodep)
{
    switch(enodep->getNodeType())
    {
        case(VAR_NODE):
            compileVarNode(dynamic_cast<VarNode *>(enodep));
        break;
        case(PRINT_NODE):
            compilePrintNode(dynamic_cast<PrintNode *>(enodep));
        break;
        case(COMPOUND_NODE):
            compileCompoundNode(dynamic_cast<CompoundNode *>(enodep));
        break;
        case(IF_NODE):
            compileIfNode(dynamic_cast<IfNode *>(enodep));
        break;
        case(WHILE_NODE):
            compileWhileNode(dynamic_cast<WhileNode *>(enodep));
        break;
    }
}
void CompilerToy::compileIfNode(IfNode *inodep)
{
    CompoundNode *condition = inodep->getCondition()->getExpression();
    BlockNode *if_block = inodep->getIfBlock();
    BlockNode *else_block = inodep->getElseBlock();
    std::string else_lbl,end_lbl,false_reg;
    else_lbl = getNextLabel();
    end_lbl = getNextLabel();
    false_reg = getNextRegister();
    writeLine("lwi " + false_reg + " 0");
    writeLine("beq " + compileCompoundNode(condition) + " " + false_reg + " _" + else_lbl);
    compileBlockNode(if_block);
    writeLine("jump _" + end_lbl);
    writeLine("vlabel " + else_lbl);
    if(else_block != NULL)
    {
        compileBlockNode(else_block);
    }
    writeLine("vlabel " + end_lbl);
}
void CompilerToy::compileWhileNode(WhileNode *wnodep)
{
    CompoundNode *condition = wnodep->getCondition()->getExpression();
    BlockNode *while_block = wnodep->getWhileBlock();
    std::string loop_label,end_label,false_reg;
    false_reg = getNextRegister();
    loop_label = getNextLabel();
    end_label = getNextLabel();
    writeLine("lwi " + false_reg + " 0");
    writeLine("vlabel " + loop_label);
    writeLine("beq " + compileCompoundNode(condition) + " " + false_reg + " _" + end_label);
    compileBlockNode(while_block);
    writeLine("jump _" + loop_label);
    writeLine("vlabel " + end_label);
}
void CompilerToy::compileVarNode(VarNode *vnodep)
{
    std::string var_reg,init_reg;
    CompoundNode *cnodep;
    var_reg = getNextRegister();
    vars.insert(std::pair<std::string,std::string>(vnodep->getVarName(),var_reg));
    cnodep = vnodep->getInitExpression();
    if(cnodep != NULL)
    {
        init_reg = compileCompoundNode(cnodep);
        writeLine("move " + var_reg + " " + init_reg);
    }
}
void CompilerToy::compilePrintNode(PrintNode *pnodep)
{
    std::string regname;
    regname = compileCompoundNode(pnodep->getExpression());
    writeLine("print " + regname);
}
std::string CompilerToy::compileCompoundNode(CompoundNode *cnodep)
{
    int i;
    CompoundNode *desc;
    CompoundNode *oper = NULL;
    CompoundNode *current;
    std::string rslt_reg(""),current_reg;
    if(cnodep->getType() == CompoundNode::COMPOUND)
    {
        for(i = 0;i < cnodep->getSize();++i)
        {
            desc = cnodep->getDesc(i);
            switch(desc->getType())
            {
                case CompoundNode::OPERATOR_PLUS:
                case CompoundNode::OPERATOR_MINUS:
                case CompoundNode::OPERATOR_MULT:
                case CompoundNode::OPERATOR_DIV:
                case CompoundNode::OPERATOR_GREATER:
                case CompoundNode::OPERATOR_GREATEREQUAL:
                case CompoundNode::OPERATOR_LESS:
                case CompoundNode::OPERATOR_LESSEQUAL:
                case CompoundNode::OPERATOR_EQUAL:
                case CompoundNode::OPERATOR_ASSIGN:
                    oper = desc;
                break;
                default:
                    if(rslt_reg == "")
                    {
                        //Other solution is better
                        if(cnodep->getDesc(i+1)->getType() == CompoundNode::OPERATOR_ASSIGN)
                        {
                            rslt_reg = compileCompoundNode(desc);
                        }
                        else
                        {
                            rslt_reg = getNextRegister();
                            writeLine("move " + rslt_reg + " " + compileCompoundNode(desc));
                        }
                    }
                    else
                    {
                        current_reg = compileCompoundNode(desc);
                        if(oper->getType() == CompoundNode::OPERATOR_PLUS)
                        {
                            writeLine("add " + rslt_reg + " " + rslt_reg + " " + current_reg);
                        }
                        else if(oper->getType() == CompoundNode::OPERATOR_MINUS)
                        {
                            writeLine("sub " + rslt_reg + " " + rslt_reg + " " + current_reg);
                        }
                        else if(oper->getType() == CompoundNode::OPERATOR_MULT)
                        {
                            writeLine("mult " + rslt_reg + " " + rslt_reg + " " + current_reg);
                        }
                        else if(oper->getType() == CompoundNode::OPERATOR_DIV)
                        {
                            writeLine("div " + rslt_reg + " " + rslt_reg + " " + current_reg);
                        }
                        else if(oper->getType() == CompoundNode::OPERATOR_EQUAL)
                        {
                            std::string done_lbl,false_lbl,true_lbl,done_reg,temp_reg,a_reg,b_reg,true_reg;
                            done_lbl = getNextLabel();
                            false_lbl = getNextLabel();
                            true_lbl = getNextLabel();
                             
                            done_reg = getNextRegister();
                            temp_reg = getNextRegister();
                            a_reg = getNextRegister();
                            b_reg = getNextRegister();
                            true_reg = getNextRegister();
                            
                            writeLine("lwi " + true_reg + " 1");
                             
                            writeLine("move " + a_reg + " " + rslt_reg);
                            writeLine("move " + b_reg + " " + current_reg);
                            writeLine("lwi " + done_reg + " 0");
                            writeLine("label " + done_lbl);
                            writeLine("label " + true_lbl);
                            writeLine("add " + a_reg + " " + a_reg + " " + done_reg);
                            writeLine("lwi " + temp_reg + " 1");
                            writeLine("beq " + done_reg + " " + true_reg + " " + done_lbl);
                            writeLine("label " + false_lbl);
                            writeLine("lwi " + temp_reg + " 0");
                            writeLine("beq " + done_reg + " " + true_reg + " " + done_lbl);
                            writeLine("label " + done_lbl);
                            writeLine("lwi " + done_reg + " 1");
                            writeLine("beq " + a_reg + " " + b_reg + " " + true_lbl);
                            writeLine("move " + rslt_reg + " " + temp_reg);
                        }
                        else if(oper->getType() == CompoundNode::OPERATOR_GREATER)
                        {
                            std::string true_lbl,false_lbl,temp_reg;
                            true_lbl = getNextLabel();
                            false_lbl = getNextLabel();
                            temp_reg = getNextRegister();
                            writeLine("lwi " + temp_reg + " 1");
                            writeLine("bg " + rslt_reg + " " + current_reg + " _" + true_lbl);
                            writeLine("lwi " + temp_reg + " 0");
                            writeLine("vlabel " + true_lbl);
                            writeLine("move " + rslt_reg + " " + temp_reg);
                        }
                        else if(oper->getType() == CompoundNode::OPERATOR_GREATEREQUAL)
                        {
                            std::string true_lbl,false_lbl,temp_reg;
                            true_lbl = getNextLabel();
                            false_lbl = getNextLabel();
                            temp_reg = getNextRegister();
                            writeLine("lwi " + temp_reg + " 1");
                            writeLine("bge " + rslt_reg + " " + current_reg + " _" + true_lbl);
                            writeLine("lwi " + temp_reg + " 0");
                            writeLine("vlabel " + true_lbl);
                            writeLine("move " + rslt_reg + " " + temp_reg);
                        }
                        else if(oper->getType() == CompoundNode::OPERATOR_LESS)
                        {
                            std::string true_lbl,false_lbl,temp_reg;
                            true_lbl = getNextLabel();
                            false_lbl = getNextLabel();
                            temp_reg = getNextRegister();
                            writeLine("lwi " + temp_reg + " 1");
                            writeLine("bl " + rslt_reg + " " + current_reg + " _" + true_lbl);
                            writeLine("lwi " + temp_reg + " 0");
                            writeLine("vlabel " + true_lbl);
                            writeLine("move " + rslt_reg + " " + temp_reg);
                        }
                        else if(oper->getType() == CompoundNode::OPERATOR_LESSEQUAL)
                        {
                            std::string true_lbl,false_lbl,temp_reg;
                            true_lbl = getNextLabel();
                            false_lbl = getNextLabel();
                            temp_reg = getNextRegister();
                            writeLine("lwi " + temp_reg + " 1");
                            writeLine("ble " + rslt_reg + " " + current_reg + " _" + true_lbl);
                            writeLine("lwi " + temp_reg + " 0");
                            writeLine("vlabel " + true_lbl);
                            writeLine("move " + rslt_reg + " " + temp_reg);
                        }
                        else if(oper->getType() == CompoundNode::OPERATOR_ASSIGN)
                        {
                            writeLine("move " + rslt_reg + " " + current_reg);
                        }
                    }
                break;
            }
        }
    }
    else if(cnodep->getType() == CompoundNode::OPERAND_INT)
    {
        rslt_reg = getNextRegister();
        writeLine("lwi " + rslt_reg + " " + cnodep->getVal());
    }
    else if(cnodep->getType() == CompoundNode::OPERAND_BOOL)
    {
        rslt_reg = getNextRegister();
        if(cnodep->getVal() == "true")
        {
            writeLine("lwi " + rslt_reg + " 1");
        }
        else
        {
            writeLine("lwi " + rslt_reg + " 0");
        }
    }
    else if(cnodep->getType() == CompoundNode::OPERAND_IDENTIFIER)
    {
        std::map<std::string,std::string>::iterator iter;
        iter = vars.find(cnodep->getVal());
        if(iter != vars.end())
        {
            return iter->second;
        }
    }
    return rslt_reg;
}
