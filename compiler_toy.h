#include <string>
#include <map>
#include <sstream>
#include "parser.h"
#ifndef _COMPILER_H_
#define _COMPILER_H_
class CompilerToy
{
    public:
        CompilerToy():regcnt(0),lblcnt(0),code(""){}
        std::string getMachineCode(BlockNode *bnodep);
        std::string getNextRegister();
        std::string getNextLabel();
        void writeLine(std::string line);
        void compileBlockNode(BlockNode *bnodep);
        void compileExpressionNode(ExpressionNode *enodep);
        void compilePrintNode(PrintNode *pnodep);
        void compileVarNode(VarNode *vnodep);
        void compileIfNode(IfNode *inodep);
        void compileWhileNode(WhileNode *wnodep);
        std::string compileCompoundNode(CompoundNode *cnodep);
    private:
        std::string code;
        int regcnt,lblcnt;
        std::map<std::string,std::string> vars;
};
#endif
