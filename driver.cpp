#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include "machine.h"
using namespace std;
int main(int argc,char *argv[])
{
    ifstream infile;
    stringstream ss;
    string line,cmd,op1,op2,op3;
    Machine machine;
    if(argc != 2)
    {
        cout << "Usage: " << argv[0] << " assemblyfilename" << endl;
        return -1;
    }
    infile.open(argv[1]);
    if(!infile)
    {
        cout << "Invalid filename" << endl;
        return -1;
    }
    while(!infile.eof())
    {
        getline(infile,line);
        if(line == "")
        {
            continue;
        }
        ss.str("");
        ss.clear();
        ss << line;
        ss >> cmd >> op1 >> op2 >> op3;
        machine.addInstruction(Instruction(cmd,op1,op2,op3));
    }
    machine.run();
    //machine.dump();
    machine.dumpErrors();
    infile.close();
    return 0;
}
