#include "machine.h"
Instruction::Instruction(std::string cmd,std::string op1)
{
    command = cmd;
    operand1 = op1;
    operand2 = "";
    operand3 = "";
}
Instruction::Instruction(std::string cmd,std::string op1,std::string op2)
{
    command = cmd;
    operand1 = op1;
    operand2 = op2;
    operand3 = "";
}
Instruction::Instruction(std::string cmd,std::string op1,std::string op2,std::string op3)
{
    command = cmd;
    operand1 = op1;
    operand2 = op2;
    operand3 = op3;
}
int Machine::resolveRegister(std::string name)
{
    std::string id;
    if(name[0] != '$')
    {
        errors.push_back(MachineError(pcount,"Not a valid register"));
        return -1;
    }
    else
    {
        return atoi(name.substr(1).c_str());
    }
}
int Machine::getRegisterContent(std::string name)
{
    if(resolveRegister(name) != -1)
    {
        if(resolveRegister(name)+1 > registers.size())
        {
            registers.resize(resolveRegister(name)+1);
        }
        return registers[resolveRegister(name)];
    }
}
void Machine::setRegisterContent(std::string name,int val)
{
    if(resolveRegister(name) != -1)
    {
        if(resolveRegister(name)+1 > registers.size())
        {
            registers.resize(resolveRegister(name)+1);
        }
        registers[resolveRegister(name)] = val;
    }
}
void Machine::addInstruction(Instruction ins)
{
    if(ins.getCmd() == "vlabel")
    {
        if(vlabels.find(ins.getOp1()) == vlabels.end())
        {
            vlabels.insert(std::pair<std::string,int>(ins.getOp1(),pcount));
        }
        else
        {
            vlabels.find(ins.getOp1())->second = pcount;
            std::cout << pcount;
        }
    }
    instructions.push_back(ins);
    ++pcount;
}
void Machine::add(Instruction ins)
{
    setRegisterContent(ins.getOp1(),getRegisterContent(ins.getOp2())+getRegisterContent(ins.getOp3()));
}
void Machine::sub(Instruction ins)
{
    setRegisterContent(ins.getOp1(),getRegisterContent(ins.getOp2())-getRegisterContent(ins.getOp3()));
}
void Machine::mult(Instruction ins)
{
    setRegisterContent(ins.getOp1(),getRegisterContent(ins.getOp2())*getRegisterContent(ins.getOp3()));
}
void Machine::div(Instruction ins)
{
    if(getRegisterContent(ins.getOp3()) == 0)
    {
        errors.push_back(MachineError(pcount,"Divide by 0"));
    }
    else
    {
        setRegisterContent(ins.getOp1(),getRegisterContent(ins.getOp2())/getRegisterContent(ins.getOp3()));
    }
}
void Machine::mod(Instruction ins)
{
    setRegisterContent(ins.getOp1(),getRegisterContent(ins.getOp2())%getRegisterContent(ins.getOp3()));
}
void Machine::addi(Instruction ins)
{
    setRegisterContent(ins.getOp1(),getRegisterContent(ins.getOp2())+atoi(ins.getOp3().c_str()));
}
void Machine::subi(Instruction ins)
{
    setRegisterContent(ins.getOp1(),getRegisterContent(ins.getOp2())-atoi(ins.getOp3().c_str()));
}
void Machine::multi(Instruction ins)
{
    setRegisterContent(ins.getOp1(),getRegisterContent(ins.getOp2())*atoi(ins.getOp3().c_str()));
}
void Machine::divi(Instruction ins)
{
    if(atoi(ins.getOp3().c_str()) == 0)
    {
        errors.push_back(MachineError(pcount,"Divide by 0"));
    }
    else
    {
        setRegisterContent(ins.getOp1(),getRegisterContent(ins.getOp2())/atoi(ins.getOp3().c_str()));
    }
}
void Machine::modi(Instruction ins)
{
    setRegisterContent(ins.getOp1(),getRegisterContent(ins.getOp2())%atoi(ins.getOp3().c_str()));
}
void Machine::andl(Instruction ins)
{
    setRegisterContent(ins.getOp1(),getRegisterContent(ins.getOp2())&getRegisterContent(ins.getOp3()));
}
void Machine::orl(Instruction ins)
{
    setRegisterContent(ins.getOp1(),getRegisterContent(ins.getOp2())|getRegisterContent(ins.getOp3()));
}
void Machine::move(Instruction ins)
{
    setRegisterContent(ins.getOp1(),getRegisterContent(ins.getOp2()));
}
void Machine::lwi(Instruction ins)
{
    setRegisterContent(ins.getOp1(),atoi(ins.getOp2().c_str()));
}
void Machine::lui(Instruction ins)
{
    setRegisterContent(ins.getOp1(),atoi(ins.getOp2().c_str())*65536);
}
void Machine::label(Instruction ins)
{
    if(labels.find(ins.getOp1()) == labels.end())
    {
        labels.insert(std::pair<std::string,int>(ins.getOp1(),pcount));
    }
    else
    {
        labels.find(ins.getOp1())->second = pcount;
    }
}
void Machine::beq(Instruction ins)
{
    if(getRegisterContent(ins.getOp1()) == getRegisterContent(ins.getOp2()))
    {
        jump(Instruction("jump",ins.getOp3()));
    }
    else
    {
        ++pcount;
    }
}
void Machine::bne(Instruction ins)
{
    if(getRegisterContent(ins.getOp1()) != getRegisterContent(ins.getOp2()))
    {
        jump(Instruction("jump",ins.getOp3()));
    }
    else
    {
        ++pcount;
    }
}
void Machine::bl(Instruction ins)
{
    if(getRegisterContent(ins.getOp1()) < getRegisterContent(ins.getOp2()))
    {
        jump(Instruction("jump",ins.getOp3()));
    }
    else
    {
        ++pcount;
    }
}
void Machine::bg(Instruction ins)
{
    if(getRegisterContent(ins.getOp1()) > getRegisterContent(ins.getOp2()))
    {
        jump(Instruction("jump",ins.getOp3()));
    }
    else
    {
        ++pcount;
    }
}
void Machine::ble(Instruction ins)
{
    if(getRegisterContent(ins.getOp1()) <= getRegisterContent(ins.getOp2()))
    {
        jump(Instruction("jump",ins.getOp3()));
    }
    else
    {
        ++pcount;
    }
}
void Machine::bge(Instruction ins)
{
    if(getRegisterContent(ins.getOp1()) >= getRegisterContent(ins.getOp2()))
    {
        jump(Instruction("jump",ins.getOp3()));
    }
    else
    {
        ++pcount;
    }
}
void Machine::jump(Instruction ins)
{
    if(ins.getOp1()[0] == '_')
    {
        if(vlabels.find(ins.getOp1().substr(1)) == vlabels.end())
        {
            errors.push_back(MachineError(pcount,"Vlabel not found"));
        }
        else
        {
            pcount = vlabels.find(ins.getOp1().substr(1))->second;
        }
    }
    else
    {
        if(labels.find(ins.getOp1()) == labels.end())
        {
            errors.push_back(MachineError(pcount,"Label not found"));
        }
        else
        {
            pcount = labels.find(ins.getOp1())->second;
        }
    }
}
void Machine::print(Instruction ins)
{
    std::cout << "CPM> " << getRegisterContent(ins.getOp1()) << std::endl;
}
void Machine::run()
{
    pcount = 0;
    while(pcount < instructions.size())
    {
        if(instructions[pcount].getCmd() == "add")
        {
            add(instructions[pcount]);
            ++pcount;
        }
        else if(instructions[pcount].getCmd() == "sub")
        {
            sub(instructions[pcount]);
            ++pcount;
        }
        else if(instructions[pcount].getCmd() == "mult")
        {
            mult(instructions[pcount]);
            ++pcount;
        }
        else if(instructions[pcount].getCmd() == "div")
        {
            div(instructions[pcount]);
            ++pcount;
        }
        else if(instructions[pcount].getCmd() == "mod")
        {
            mod(instructions[pcount]);
            ++pcount;
        }
        else if(instructions[pcount].getCmd() == "addi")
        {
            addi(instructions[pcount]);
            ++pcount;
        }
        else if(instructions[pcount].getCmd() == "subi")
        {
            subi(instructions[pcount]);
            ++pcount;
        }
        else if(instructions[pcount].getCmd() == "multi")
        {
            multi(instructions[pcount]);
            ++pcount;
        }
        else if(instructions[pcount].getCmd() == "divi")
        {
            divi(instructions[pcount]);
            ++pcount;
        }
        else if(instructions[pcount].getCmd() == "modi")
        {
            modi(instructions[pcount]);
            ++pcount;
        }
        else if(instructions[pcount].getCmd() == "andl")
        {
            andl(instructions[pcount]);
            ++pcount;
        }
        else if(instructions[pcount].getCmd() == "orl")
        {
            orl(instructions[pcount]);
            ++pcount;
        }
        else if(instructions[pcount].getCmd() == "move")
        {
            move(instructions[pcount]);
            ++pcount;
        }
        else if(instructions[pcount].getCmd() == "lwi")
        {
            lwi(instructions[pcount]);
            ++pcount;
        }
        else if(instructions[pcount].getCmd() == "lui")
        {
            lui(instructions[pcount]);
            ++pcount;
        }
        else if(instructions[pcount].getCmd() == "label")
        {
            label(instructions[pcount]);
            ++pcount;
        }
        else if(instructions[pcount].getCmd() == "beq")
        {
            beq(instructions[pcount]);
        }
        else if(instructions[pcount].getCmd() == "bne")
        {
            bne(instructions[pcount]);
        }
        else if(instructions[pcount].getCmd() == "bl")
        {
            bl(instructions[pcount]);
        }
        else if(instructions[pcount].getCmd() == "bg")
        {
            bg(instructions[pcount]);
        }
        else if(instructions[pcount].getCmd() == "ble")
        {
            ble(instructions[pcount]);
        }
        else if(instructions[pcount].getCmd() == "bge")
        {
            bge(instructions[pcount]);
        }
        else if(instructions[pcount].getCmd() == "jump")
        {
            jump(instructions[pcount]);
        }
        else if(instructions[pcount].getCmd() == "print")
        {
            print(instructions[pcount]);
            ++pcount;
        }
        else if(instructions[pcount].getCmd() == "vlabel")
        {
            ++pcount;
        }
        else
        {
            errors.push_back(MachineError(pcount,"Invalid command"));
            ++pcount;
        }
    }
}
void Machine::dumpErrors()
{
    int i;
    for(i = 0;i < errors.size();++i)
    {
        std::cout << "Command: " << errors[i].getSerial()+1 << ": " << errors[i].getMesg() << std::endl;
    }
}
void Machine::dump()
{
    int i;
    std::cout << "Program Count: " << pcount << std::endl;
    for(i = 0;i < instructions.size();++i)
    {
        std::cout << instructions[i].getCmd() << '\t' << instructions[i].getOp1() << '\t' <<  instructions[i].getOp2() << '\t' << instructions[i].getOp3() << std::endl;
    }
}
