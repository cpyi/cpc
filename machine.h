#include <iostream>
#include <string>
#include <vector>
#include <map>
#ifndef _MACHINE_H_
#define _MACHINE_H_
class Instruction
{
    public:
        Instruction(std::string cmd,std::string op1);
        Instruction(std::string cmd,std::string op1,std::string op2);
        Instruction(std::string cmd,std::string op1,std::string op2,std::string op3);
        std::string getCmd()
        {
            return command;
        }
        std::string getOp1()
        {
            return operand1;
        }
        std::string getOp2()
        {
            return operand2;
        }
        std::string getOp3()
        {
            return operand3;
        }
    private:
        std::string command;
        std::string operand1;
        std::string operand2;
        std::string operand3;
};
class MachineError
{
    public:
        MachineError(int serial,std::string mesg)
            :serial(serial),mesg(mesg){}
        int getSerial()
        {
            return serial;
        }
        std::string getMesg()
        {
            return mesg;
        }
    private:
        int serial;
        std::string mesg;
};
class Machine
{
    public:
        Machine():pcount(0){}
        int resolveRegister(std::string name);
        int getRegisterContent(std::string name);
        void setRegisterContent(std::string name,int val);
        void addInstruction(Instruction);

        //Arithmatic
        void add(Instruction);
        void sub(Instruction);
        void mult(Instruction);
        void div(Instruction);
        void mod(Instruction);
        void addi(Instruction);
        void subi(Instruction);
        void multi(Instruction);
        void divi(Instruction);
        void modi(Instruction);

        //Logic
        void andl(Instruction);
        void orl(Instruction);
        
        //Data Transfer
        void move(Instruction);
        void lwi(Instruction);
        void lui(Instruction);
        
        //Conditional Branch
        void label(Instruction);
        void beq(Instruction);
        void bne(Instruction);
        void bl(Instruction);
        void bg(Instruction);
        void ble(Instruction);
        void bge(Instruction);
        
        //Unconditional jump
        void jump(Instruction);
        
        //Print
        void print(Instruction);
        
        void run();
        void dump();
        void dumpErrors();
    private:
        std::vector<Instruction> instructions;
        std::vector<int> registers;
        std::vector<MachineError> errors;
        int pcount;
        std::map<std::string,int> labels;
        std::map<std::string,int> vlabels;
};
#endif
