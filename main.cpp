#include <iostream>
#include <fstream>
#include "reader.h"
#include "scanner.h"
#include "parser.h"
#include "analyser.h"
#include "compiler_toy.h"
#include "compiler_51.h"
using namespace std;
int main(int argc,char *argv[])
{
    //Convert input file to a string
    fstream in_file,out_file;
    char ch;
    string str;
    if(argc != 2)
    {
        cout << "Usage: " << argv[0] << " scriptin_file" << endl;
        return 1;
    }
    in_file.open(argv[1],ios::in);
    out_file.open("target.cps",ios::out);
    if(!in_file)
    {
        cout << "File Open Error." << endl;
        return 1;
    }
    while(!in_file.eof())
    {
        in_file.get(ch);
        str.push_back(ch);
    }
    //Reader
    Reader reader(str);
    Scanner scanner(reader);
    Parser parser(scanner);
    BlockNode *bnodep;
    //Reader Test
    /*
    std::cout << reader.nextChar();
    std::cout << reader.nextChar();
    std::cout << reader.nextChar();
    std::cout << reader.prevChar();
    */
    //Token Test
    /*
    Token token;
    cout << "Tokens:" << endl;
    do
    {
        token = parser.nextToken();
        token.dump();
        cout << endl;
    }
    while(token.getType() != Token::EOF_TOKEN);
    */
    //Parse Test
    bnodep = parser.parse();
    bnodep->dump(0);
    parser.dumpError();
    //Analysis Test
    Analyser analyser;
    analyser.analyzeBlockNode(bnodep);
    analyser.dumpError();
    //Compiler Test
    Compiler51 compiler;
    out_file <<  compiler.getMachineCode(bnodep);
    return 0;
}
