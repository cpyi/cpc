#include "parser.h"
void ExpressionNode::setLine(int n)
{
    line = n;
}
int ExpressionNode::getLine()
{
    return line;
}
void BlockNode::addExpression(ExpressionNode *enodep)
{
    expressions.push_back(enodep);
}
int BlockNode::getExpressionNum()
{
    expressions.size();
}
ExpressionNode* BlockNode::getExpression(int index)
{
    return expressions[index];
}
void PrintNode::setExpression(CompoundNode *cnodep)
{
    print_expression = cnodep;
}
void NumberNode::setNumber(std::string n)
{
    number = n;
}
Token Parser::nextToken()
{
    Token token;
    if(lookahead_consumed)
    {
        do
        {
            token = scanner.nextToken();
        }
        while(token.getType() == Token::LINECOMMENT_TOKEN || token.getType() == Token::BLOCKCOMMENT_TOKEN);
        current_token = token;
        return current_token;
    }
    else
    {
        current_token = lookahead_token;
        lookahead_consumed = true;
        return current_token;
    }
}
Token Parser::lookAhead()
{
    Token token;
    if(lookahead_consumed)
    {
        do
        {
            token = scanner.nextToken();
        }
        while(token.getType() == Token::LINECOMMENT_TOKEN || token.getType() == Token::BLOCKCOMMENT_TOKEN);
        lookahead_token = token;
        lookahead_consumed = false;
        return lookahead_token;
    }
    else
    {
        return lookahead_token;
    }
}
BlockNode* Parser::parse()
{
    return parseBlock();
}
BlockNode* Parser::parseBlock()
{
    BlockNode *bnodep = new BlockNode();
    ExpressionNode *enodep;
    while(lookAhead().getType() != Token::RIGHTBRACE_TOKEN && lookAhead().getType() != Token::EOF_TOKEN)
    {
        enodep = parseExpression();
        if(enodep != NULL)
        {
            bnodep->addExpression(enodep);
        }
    }
    //skip `}`
    nextToken();

    return bnodep;
}
ExpressionNode* Parser::parseExpression()
{
    ExpressionNode *enodep;
    CompoundNode *cnodep;
    PrintNode *pnodep;
    NumberNode *nnodep;
    switch(lookAhead().getType())
    {
        case Token::PRINT_TOKEN:
            //Consume `print`
            nextToken();

            cnodep = parseCompoundExpression(0);
            if(enodep == NULL)
            {
                errors.push_back(ParserError(scanner.getLine(),"Missing expression after `print`"));
            }
            pnodep = new PrintNode();
            pnodep->setLine(scanner.getLine());
            pnodep->setExpression(cnodep);
            matchSemicolon();
            return pnodep;
        break;
        case Token::INTEGER_TOKEN:
        case Token::IDENTIFIER_TOKEN:
        case Token::LEFTPAREN_TOKEN:
            return parseCompoundExpression(0);
        break;
        case Token::VAR_TOKEN:
            return parseVarExpression();
        break;
        case Token::IF_TOKEN:
            return parseIfExpression();
        break;
        case Token::WHILE_TOKEN:
            return parseWhileExpression();
        break;
        default:
            nextToken();
            return NULL;
        break;
    }
}
VarNode* Parser::parseVarExpression()
{
    //Consume `var`
    nextToken();

    std::string name;
    std::string type;
    CompoundNode *cnodep = NULL;
    VarNode *vnodep;
    if(lookAhead().getType() == Token::IDENTIFIER_TOKEN)
    {
        //Consume Identifier
        nextToken();

        name = current_token.getContext();
        if(lookAhead().getType() != Token::COLON_TOKEN)
        {
            errors.push_back(ParserError(scanner.getLine(),"Missing colon after `var` and identifier"));
            return NULL;
        }
        else
        {
            nextToken();
        }
        if(lookAhead().getType() != Token::TYPE_TOKEN)
        {
            errors.push_back(ParserError(scanner.getLine(),"Missing type in `var` declaration"));
            return NULL;
        }
        nextToken();
        type = current_token.getContext();
        if(lookAhead().getType() == Token::ASSIGN_TOKEN)
        {
            //consume `=`
            nextToken();
            cnodep = parseCompoundExpression(0);
            matchSemicolon();
            vnodep = new VarNode(name,type,cnodep);
            vnodep->setLine(scanner.getLine());
            return vnodep;
        }
        else
        {
            cnodep = NULL;
            matchSemicolon();
            vnodep = new VarNode(name,type,cnodep);
            vnodep->setLine(scanner.getLine());
            return vnodep;
        }
    }
    else
    {
        errors.push_back(ParserError(scanner.getLine(),"`var` should follow a identifier"));
        return NULL;
    }
}
IfNode* Parser::parseIfExpression()
{
    //skip `if`
    nextToken();

    IfNode *inodep = new IfNode();
    ParenNode *pnodep;
    BlockNode *bnodep;
    pnodep = parseParenExpression();
    if(pnodep == NULL)
    {
        errors.push_back(ParserError(scanner.getLine(),"Missing condition after `if`"));
        inodep->setCondition(NULL);
    }
    else
    {
        inodep->setCondition(pnodep);
    }
    bnodep = parseBlock();
    inodep->setIfBlock(bnodep);
    if(lookAhead().getType() == Token::ELSE_TOKEN)
    {
        //consume `else`
        nextToken();

        bnodep = parseBlock();
        inodep->setElseBlock(bnodep);
    }
    else
    {
        inodep->setElseBlock(NULL);
    }
    inodep->setLine(scanner.getLine());
    return inodep;
}
void ParenNode::setExpression(CompoundNode *cnodep)
{
    paren_expression = cnodep;
}
void ParenNode::dump(int offset)
{
    dumpOffset(offset);
    std::cout << "ParenNode>" << std::endl;
    if(paren_expression != NULL)
    {
        paren_expression->dump(offset+5);
    }
    else
    {
        dumpOffset(offset+5);
        std::cout << "EMPTY" << std::endl;
    }
}
void IfNode::setCondition(ParenNode *pnodep)
{
    condition = pnodep;
}
void IfNode::setIfBlock(BlockNode *bnodep)
{
    if_block = bnodep;
}
void IfNode::setElseBlock(BlockNode *bnodep)
{
    else_block = bnodep;
}
ParenNode* Parser::parseParenExpression()
{
    ParenNode *pnodep;
    CompoundNode *cnodep;
    if(lookAhead().getType() != Token::LEFTPAREN_TOKEN)
    {
        return NULL;
    }
    else
    {
        //skip `(`
        nextToken();

        cnodep = parseCompoundExpression(0);
        pnodep = new ParenNode();
        pnodep->setExpression(cnodep);
    }
    if(lookAhead().getType() != Token::RIGHTPAREN_TOKEN)
    {
        errors.push_back(ParserError(scanner.getLine(),"Missing right parenthesis"));
    }
    else
    {
        //skip `)`
        nextToken();

    }
    return pnodep;
}
WhileNode* Parser::parseWhileExpression()
{
    //skip `while`
    nextToken();

    WhileNode *wnodep = new WhileNode();
    ParenNode *pnodep;
    BlockNode *bnodep;
    pnodep = parseParenExpression();
    if(pnodep == NULL)
    {
        errors.push_back(ParserError(scanner.getLine(),"Missing condition after `while`"));
        wnodep->setCondition(NULL);
    }
    else
    {
        wnodep->setCondition(pnodep);
    }
    bnodep = parseBlock();
    wnodep->setWhileBlock(bnodep);
    wnodep->setLine(scanner.getLine());
    return wnodep;
}
void Parser::matchSemicolon()
{
    if(lookAhead().getType() == Token::SEMICOLON_TOKEN)
    {
        nextToken();
    }
    else
    {
        errors.push_back(ParserError(scanner.getLine(),"Expected semicolon at end of expression"));
    }
}
//dump functions
void ExpressionNode::dump(int offset)
{
    dumpOffset(offset);
    std::cout << "ExpressionNode>" << std::endl;
}
void PrintNode::dump(int offset)
{
    dumpOffset(offset);
    std::cout << "PrintNode>" << std::endl;
    if(print_expression != NULL)
    {
        print_expression->dump(offset+5);
    }
    else
    {
        dumpOffset(offset);
        std::cout << "EMPTY" << std::endl;
    }
}
void NumberNode::dump(int offset)
{
    dumpOffset(offset);
    std::cout << "NumberNode>" << std::endl;
    dumpOffset(offset+5);
    std::cout << number << std::endl;
}
void VarNode::dump(int offset)
{
    dumpOffset(offset);
    std::cout << "VarNode>" << std::endl;
    dumpOffset(offset+5);
    std::cout << "Name: " << name << std::endl;
    dumpOffset(offset+5);
    std::cout << "Type: " << type << std::endl;
    if(init_expression != NULL)
    {
        init_expression->dump(offset+5);
    }
    else
    {
        dumpOffset(offset+5);
        std::cout << "Init: EMPTY" << std::endl;
    }
}
void IfNode::dump(int offset)
{
    dumpOffset(offset);
    std::cout << "IfNode>" << std::endl;
    dumpOffset(offset+5);
    std::cout << "Condition:" << std::endl;
    if(condition != NULL)
    {
        condition->dump(offset+10);
    }
    else
    {
        dumpOffset(offset+5);
        std::cout << "EMPTY" << std::endl;
    }
    dumpOffset(offset+5);
    std::cout << "IfBlock:" << std::endl;
    if(if_block != NULL)
    {
        if_block->dump(offset+10);
    }
    else
    {
        dumpOffset(offset+5);
        std::cout << "EMPTY" << std::endl;
    }
    dumpOffset(offset+5);
    std::cout << "ElseBlock:" << std::endl;
    if(else_block != NULL)
    {
        else_block->dump(offset+10);
    }
    else
    {
        dumpOffset(offset+5);
        std::cout << "EMPTY" << std::endl;
    }
}
void BlockNode::dumpOffset(int offset)
{
    int i;
    for(i = 0;i < offset;++i)
    {
        std::cout << " ";
    }
}
void BlockNode::dump(int offset)
{
    int i;
    dumpOffset(offset);
    std::cout << "BlockNode(" << expressions.size() << ")>" << std::endl;
    for(i = 0;i < expressions.size();++i)
    {
        if(expressions[i] != NULL)
        {
            expressions[i]->dump(offset+5);
        }
        else
        {
            dumpOffset(offset+5);
            std::cout << "EMPTY" << std::endl;
        }
    }
}
void ExpressionNode::dumpOffset(int offset)
{
    int i;
    for(i = 0;i < offset;++i)
    {
        std::cout << " ";
    }
}
void WhileNode::setCondition(ParenNode *pnodep)
{
    condition = pnodep;
}
void WhileNode::setWhileBlock(BlockNode *bnodep)
{
    while_block = bnodep;
}
void WhileNode::dump(int offset)
{
    dumpOffset(offset);
    std::cout << "WhileNode>" << std::endl;
    dumpOffset(offset+5);
    std::cout << "Condition:" << std::endl;
    if(condition != NULL)
    {
        condition->dump(offset+10);
    }
    else
    {
        dumpOffset(offset+5);
        std::cout << "EMPTY" << std::endl;
    }
    dumpOffset(offset+5);
    std::cout << "WhileBlock:" << std::endl;
    if(while_block != NULL)
    {
        while_block->dump(offset+10);
    }
    else
    {
        dumpOffset(offset+5);
        std::cout << "EMPTY" << std::endl;
    }
}
void CompoundNode::setType(CompoundNode::CompoundType tp)
{
    type = tp;
}
int CompoundNode::getSize()
{
    return desc.size();
}
CompoundNode* CompoundNode::getDesc(int index)
{
    return desc[index];
}
CompoundNode::CompoundType CompoundNode::getType()
{
    return type;
}
void CompoundNode::setVal(std::string str)
{
    val = str;
}
std::string CompoundNode::getVal()
{
    return val;
}
void CompoundNode::addDesc(CompoundNode *cnodep)
{
    desc.push_back(cnodep);
}
CompoundNode* Parser::parseOperand()
{
    CompoundNode *cnodep = new CompoundNode();
    //Consume the operand
    nextToken();

    switch(current_token.getType())
    {
        case Token::INTEGER_TOKEN:
            cnodep->setType(CompoundNode::OPERAND_INT);
            cnodep->setVal(current_token.getContext());
        break;
        case Token::BOOL_TOKEN:
            cnodep->setType(CompoundNode::OPERAND_BOOL);
            cnodep->setVal(current_token.getContext());
        break;
        case Token::IDENTIFIER_TOKEN:
            cnodep->setType(CompoundNode::OPERAND_IDENTIFIER);
            cnodep->setVal(current_token.getContext());
        break;
        case Token::LEFTPAREN_TOKEN:
            cnodep = parseCompoundExpression(0);
            if(lookAhead().getType() == Token::RIGHTPAREN_TOKEN)
            {
                nextToken();
            }
            else
            {
                errors.push_back(ParserError(scanner.getLine(),"Missing right parenthesis"));
            }
        break;
        case Token::MINUS_TOKEN:
            nextToken();
            if(current_token.getType() != Token::INTEGER_TOKEN)
            {
                errors.push_back(ParserError(scanner.getLine(),"Unexpected token"));
            }
            else
            {
                cnodep->setType(CompoundNode::OPERAND_INT);
                cnodep->setVal("-" + current_token.getContext());
            }
        break;
        default:
            errors.push_back(ParserError(scanner.getLine(),"Unexpected token"));
            current_token.dump();
            return NULL;
        break;
    }
    return cnodep;
}
int Parser::getBindingPower(Token token)
{
    switch(token.getType())
    {
        case Token::ASSIGN_TOKEN:
            return 100;
        break;
        case Token::GREATER_TOKEN:
        case Token::GREATEREQUAL_TOKEN: 
        case Token::LESS_TOKEN:
        case Token::LESSEQUAL_TOKEN:
        case Token::EQUAL_TOKEN:
            return 110;
        break;
        case Token::PLUS_TOKEN:
        case Token::MINUS_TOKEN:
            return 120;
        break;
        case Token::MULT_TOKEN:
        case Token::DIV_TOKEN:
            return 130;
        break;
        default:
            return -1;
        break;
    }
}
CompoundNode* Parser::createOperatorNode(Token token)
{
    CompoundNode *cnodep;
    switch(token.getType())
    {
        case Token::PLUS_TOKEN:
            cnodep = new CompoundNode();
            cnodep->setType(CompoundNode::OPERATOR_PLUS);
        break;
        case Token::MINUS_TOKEN:
            cnodep = new CompoundNode();
            cnodep->setType(CompoundNode::OPERATOR_MINUS);
        break;
        case Token::MULT_TOKEN:
            cnodep = new CompoundNode();
            cnodep->setType(CompoundNode::OPERATOR_MULT);
        break;
        case Token::DIV_TOKEN:
            cnodep = new CompoundNode();
            cnodep->setType(CompoundNode::OPERATOR_DIV);
        break;
        case Token::GREATER_TOKEN:
            cnodep = new CompoundNode();
            cnodep->setType(CompoundNode::OPERATOR_GREATER);
        break;
        case Token::GREATEREQUAL_TOKEN:
            cnodep = new CompoundNode();
            cnodep->setType(CompoundNode::OPERATOR_GREATEREQUAL);
        break;
        case Token::LESS_TOKEN:
            cnodep = new CompoundNode();
            cnodep->setType(CompoundNode::OPERATOR_LESS);
        break;
        case Token::LESSEQUAL_TOKEN:
            cnodep = new CompoundNode();
            cnodep->setType(CompoundNode::OPERATOR_LESSEQUAL);
        break;
        case Token::EQUAL_TOKEN:
            cnodep = new CompoundNode();
            cnodep->setType(CompoundNode::OPERATOR_EQUAL);
        break;
        case Token::ASSIGN_TOKEN:
            cnodep = new CompoundNode();
            cnodep->setType(CompoundNode::OPERATOR_ASSIGN);
        break;
        default:
            cnodep = NULL;
        break;
    }
    return cnodep;
}
CompoundNode* Parser::parseCompoundExpression(int rbp)
{
    CompoundNode *operand,*compound,*node;
    Token operator_token,oper;
    int lbp;
    operand = parseOperand();
    if(operand == NULL)
    {
        return NULL;
    }
    compound = new CompoundNode();
    compound->setLine(scanner.getLine());
    compound->setType(CompoundNode::COMPOUND);
    compound->addDesc(operand);
    operator_token = lookAhead();
    lbp = getBindingPower(operator_token);
    if(lbp == -1)
    {
        delete compound;
        return operand;
    }
    while(rbp < lbp)
    {
        operator_token = nextToken();
        compound->addDesc(createOperatorNode(operator_token));
        node = parseCompoundExpression(lbp);
        compound->addDesc(node);
        oper = lookAhead();
        lbp = getBindingPower(oper);
        if(lbp == -1)
        {
            return compound;
        }
    }
    return compound;
}
void CompoundNode::dump(int offset)
{
    dumpOffset(offset);
    int i;
    std::cout << "CompoundNode>" << std::endl;
    if(type == COMPOUND)
    {
        dumpOffset(offset+5);
        std::cout << "COMPOUND" << std::endl;
        for(i = 0;i < desc.size();++i)
        {
            if(desc[i] != NULL)
            {
                desc[i]->dump(offset+10);
            }
        }
    }
    else
    {
        switch(type)
        {
            case OPERAND_INT:
                dumpOffset(offset+5);
                std::cout << "OPERAND_INT(" << val << ")" <<  std::endl;
            break;
            case OPERAND_BOOL:
                dumpOffset(offset+5);
                std::cout << "OPERAND_BOOL(" << val << ")" <<  std::endl;
            break;
            case OPERAND_IDENTIFIER:
                dumpOffset(offset+5);
                std::cout << "OPERAND_IDENTIFIER(" << val << ")" << std::endl;
            break;
            case OPERATOR_PLUS:
                dumpOffset(offset+5);
                std::cout << "OPERATOR_PLUS" << std::endl;
            break;
            case OPERATOR_MINUS:
                dumpOffset(offset+5);
                std::cout << "OPERATOR_MINUS" << std::endl;
            break;
            case OPERATOR_MULT:
                dumpOffset(offset+5);
                std::cout << "OPERATOR_MULT" << std::endl;
            break;
            case OPERATOR_DIV:
                dumpOffset(offset+5);
                std::cout << "OPERATOR_DIV" << std::endl;
            break;
            case OPERATOR_GREATER:
                dumpOffset(offset+5);
                std::cout << "OPERATOR_GREATER" << std::endl;
            break;
            case OPERATOR_GREATEREQUAL:
                dumpOffset(offset+5);
                std::cout << "OPERATOR_GREATEREQUAL" << std::endl;
            break;
            case OPERATOR_LESS:
                dumpOffset(offset+5);
                std::cout << "OPERATOR_LESS" << std::endl;
            break;
            case OPERATOR_LESSEQUAL:
                dumpOffset(offset+5);
                std::cout << "OPERATOR_LESSEQUAL" << std::endl;
            break;
            case OPERATOR_EQUAL:
                dumpOffset(offset+5);
                std::cout << "OPERATOR_EQUAL" << std::endl;
            break;
            case OPERATOR_ASSIGN:
                dumpOffset(offset+5);
                std::cout << "OPERATOR_ASSIGN" << std::endl;
            break;
        }
    }
}
void Parser::dumpError()
{
    int i;
    for(i = 0;i < errors.size();++i)
    {
        errors[i].dump(0);
    }
}
void ParserError::dumpOffset(int offset)
{
    int i;
    for(i = 0;i < offset;++i)
    {
        std::cout << " ";
    }
}
void ParserError::dump(int offset)
{
    dumpOffset(offset);
    std::cout << "Line: " << line+1 << ": " << mesg << std::endl;
}
