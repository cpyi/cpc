#include <iostream>
#include <string>
#include <vector>
#include "scanner.h"
#ifndef _PARSER_H_
#define _PARSER_H_
enum NodeType
{
    EXPRESSION_NODE,BLOCK_NODE,COMPOUND_NODE,
    PRINT_NODE,NUMBER_NODE,ASSIGN_NODE,
    VAR_NODE,PAREN_NODE,IF_NODE,WHILE_NODE
};
class ExpressionNode
{
    public:
        void dumpOffset(int offset);
        virtual void dump(int offset);
        virtual NodeType getNodeType()
        {
            return EXPRESSION_NODE;
        }
        void setLine(int);
        int getLine();
    private:
        int line;
};
class BlockNode
{
    public:
        void addExpression(ExpressionNode *);
        int getExpressionNum();
        ExpressionNode* getExpression(int index);
        void dumpOffset(int offset);
        void dump(int offset);
        virtual NodeType getNodeType()
        {
            return BLOCK_NODE;
        }
    private:
        std::vector<ExpressionNode *> expressions;
};
class CompoundNode:public ExpressionNode
{
    public:
        enum CompoundType
        {
            OPERAND_INT,OPERAND_BOOL,OPERAND_IDENTIFIER,
            OPERATOR_PLUS,OPERATOR_MINUS,OPERATOR_MULT,OPERATOR_DIV,
            OPERATOR_GREATER,OPERATOR_GREATEREQUAL,
            OPERATOR_LESS,OPERATOR_LESSEQUAL,
            OPERATOR_EQUAL,OPERATOR_ASSIGN,
            COMPOUND
        };
        void setType(CompoundType);
        CompoundType getType();
        void setVal(std::string);
        std::string getVal();
        void addDesc(CompoundNode *);
        int getSize();
        CompoundNode* getDesc(int index);
        virtual void dump(int offset);
        virtual NodeType getNodeType()
        {
            return COMPOUND_NODE;
        }
    private:
        CompoundType type;
        std::string val;
        std::vector<CompoundNode *> desc;
};
class PrintNode:public ExpressionNode
{
    public:
        void setExpression(CompoundNode *enodep);
        CompoundNode* getExpression()
        {
            return print_expression;
        }
        virtual void dump(int offset);
        virtual NodeType getNodeType()
        {
            return PRINT_NODE;
        }
    private:
        CompoundNode *print_expression;
};
class NumberNode:public ExpressionNode
{
    public:
        void setNumber(std::string);
        virtual void dump(int offset);
        virtual NodeType getNodeType()
        {
            return NUMBER_NODE;
        }
    private:
        std::string number;
};
class VarNode:public ExpressionNode
{
    public:
        VarNode(std::string name,std::string type,CompoundNode *cnodep)
            :name(name),type(type),init_expression(cnodep){}
        std::string getVarName()
        {
            return name;
        }
        std::string getVarType()
        {
            return type;
        }
        CompoundNode *getInitExpression()
        {
            return init_expression;
        }
        virtual void dump(int offset);
        virtual NodeType getNodeType()
        {
            return VAR_NODE;
        }
    private:
        std::string name;
        std::string type;
        CompoundNode *init_expression;
};
class ParenNode:public ExpressionNode
{
    public:
        void setExpression(CompoundNode *enodep);
        CompoundNode* getExpression()
        {
            return paren_expression;
        }
        virtual void dump(int offset);
        virtual NodeType getNodeType()
        {
            return PAREN_NODE;
        }
    private:
        CompoundNode *paren_expression;
};
class IfNode:public ExpressionNode
{
    public:
        void setCondition(ParenNode *);
        ParenNode* getCondition()
        {
            return condition;
        }
        void setIfBlock(BlockNode *);
        BlockNode* getIfBlock()
        {
            return if_block;
        }
        void setElseBlock(BlockNode *);
        BlockNode* getElseBlock()
        {
            return else_block;
        }
        virtual void dump(int offset);
        virtual NodeType getNodeType()
        {
            return IF_NODE;
        }
    private:
        ParenNode *condition;
        BlockNode *if_block;
        BlockNode *else_block;
};
class WhileNode:public ExpressionNode
{
    public:
        void setCondition(ParenNode *);
        ParenNode* getCondition()
        {
            return condition;
        }
        void setWhileBlock(BlockNode *);
        BlockNode* getWhileBlock()
        {
            return while_block;
        }
        virtual void dump(int offset);
        virtual NodeType getNodeType()
        {
            return WHILE_NODE;
        }
    private:
        ParenNode *condition;
        BlockNode *while_block;
};
class ParserError
{
    public:
        ParserError(){}
        ParserError(int line,std::string mesg):line(line),mesg(mesg){}
        void dumpOffset(int);
        void dump(int);
    private:
        int line;
        std::string mesg;
};
class Parser
{
    public:
        Parser(Scanner scanner)
            :scanner(scanner),lookahead_consumed(true){}
        Token nextToken();
        Token lookAhead();
        BlockNode* parse();
        BlockNode* parseBlock();
        ExpressionNode* parseExpression();
        VarNode* parseVarExpression();
        ParenNode* parseParenExpression();
        IfNode* parseIfExpression();
        WhileNode* parseWhileExpression();
        CompoundNode* parseOperand();
        CompoundNode* createOperatorNode(Token);
        CompoundNode* parseCompoundExpression(int rbp);
        int getBindingPower(Token);
        void skipError();//NIY
        void matchSemicolon();
        void dumpError();
    private:
        Scanner scanner;
        Token current_token;
        Token lookahead_token;
        bool lookahead_consumed;
        std::vector<ParserError> errors;
};
#endif
