#include "reader.h"
#include <iostream>
Reader::Reader(std::string s)
{
    str = s;
    pos = 0;
}
char Reader::nextChar()
{
    return pos == str.size() ? '\0' : str[pos++];
}
char Reader::prevChar()
{
    return pos == 0 ? str[0] : str[--pos];
}
