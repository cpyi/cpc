#include <string>
#ifndef _READER_H_
#define _READER_H_
class Reader
{
    public:
        Reader(std::string s);
        char nextChar();
        char prevChar();
    private:
        std::string str;
        int pos;
};
#endif
