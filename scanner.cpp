#include "scanner.h"
void Token::dump()
{
    switch(token_type)
    {
        case EOF_TOKEN:
            std::cout << "EOF_TOKEN";
        break;
        case COLON_TOKEN:
            std::cout << "COLON_TOKEN";
        break;
        case SEMICOLON_TOKEN:
            std::cout << "SEMICOLON_TOKEN";
        break;
        case LEFTPAREN_TOKEN:
            std::cout << "LEFTPAREN_TOKEN";
        break;
        case RIGHTPAREN_TOKEN:
            std::cout << "RIGHTPAREN_TOKEN";
        break;
        case LEFTBRACE_TOKEN:
            std::cout << "LEFTBRACE_TOKEN";
        break;
        case RIGHTBRACE_TOKEN:
            std::cout << "RIGHTBRACE_TOKEN";
        break;
        case IDENTIFIER_TOKEN:
            std::cout << "IDENTIFIER_TOKEN" << "(" << token_context << ")";
        break;
        case PLUS_TOKEN:
            std::cout << "PLUS_TOKEN";
        break;
        case PLUSPLUS_TOKEN:
            std::cout << "PLUSPLUS_TOKEN";
        break;
        case PLUSASSIGN_TOKEN:
            std::cout << "PLUSASSIGN_TOKEN";
        break;
        case MINUS_TOKEN:
            std::cout << "MINUS_TOKEN";
        break;
        case MINUSMINUS_TOKEN:
            std::cout << "MINUSMINUS_TOKEN";
        break;
        case MINUSASSIGN_TOKEN:
            std::cout << "MINUSASSIGN_TOKEN";
        break;
        case MULT_TOKEN:
            std::cout << "MULT_TOKEN";
        break;
        case DIV_TOKEN:
            std::cout << "DIV_TOKEN";
        break;
        case ASSIGN_TOKEN:
            std::cout << "ASSIGN_TOKEN";
        break;
        case EQUAL_TOKEN:
            std::cout << "EQUAL_TOKEN";
        break;
        case NOTEQUAL_TOKEN:
            std::cout << "NOTEQULA_TOKEN";
        break;
        case GREATER_TOKEN:
            std::cout << "GREATER_TOKEN";
        break;
        case GREATEREQUAL_TOKEN:
            std::cout << "GREATEREQUAL_TOKEN";
        break;
        case LESS_TOKEN:
            std::cout << "LESS_TOKEN";
        break;
        case LESSEQUAL_TOKEN:
            std::cout << "LESSEQUAL_TOKEN";
        break;
        case AND_TOKEN:
            std::cout << "AND_TOKEN";
        break;
        case OR_TOKEN:
            std::cout << "OR_TOKEN";
        break;
        case NOT_TOKEN:
            std::cout << "NOT_TOKEN";
        break;
        case LINECOMMENT_TOKEN:
            std::cout << "LINECOMMENT_TOKEN" << "(" << token_context << ")";
        break;
        case BLOCKCOMMENT_TOKEN:
            std::cout << "BLOCKCOMMENT_TOKEN" << "(" << token_context << ")";
        break;
        case TYPE_TOKEN:
            std::cout << "TYPE_TOKEN" << "(" << token_context << ")";
        break;
        case BOOL_TOKEN:
            std::cout << "BOOL_TOKEN" << "(" << token_context << ")";
        break;
        case INTEGER_TOKEN:
            std::cout << "INTEGER_TOKEN" << "(" << token_context << ")";
        break;
        case VAR_TOKEN:
            std::cout << "VAR_TOKEN";
        break;
        case IF_TOKEN:
            std::cout << "IF_TOKEN";
        break;
        case ELSE_TOKEN:
            std::cout << "ELSE_TOKEN";
        break;
        case WHILE_TOKEN:
            std::cout << "WHILE_TOKEN";
        break;
        case PRINT_TOKEN:
            std::cout << "PRINT_TOKEN";
        break;
    }
}
Token::TokenType Token::getType()
{
    return token_type;
}
std::string Token::getContext()
{
    return token_context;
}
void Scanner::matchLineEnd()
{
    //Match end of line for different platform "\r\n" for windows "\n" for linux "\r" for mac
    if(reader.nextChar() == '\r')
    {
        if(reader.nextChar() == '\n')
        {
            ++line;
        }
        else
        {
            reader.prevChar();
            ++line;
        }
    }
    else if(reader.nextChar() == '\n')
    {
        ++line;
    }
    else
    {
        reader.prevChar();
    }
}
Token Scanner::nextToken()
{
    while(true)
    {
        switch(state)
        {
            case START_STATE:
                switch(reader.nextChar())
                {
                    case '\0' :
                        return Token(Token::EOF_TOKEN);
                    break;
                    case ';' :
                        return Token(Token::SEMICOLON_TOKEN);
                    break;
                    case ':' :
                        return Token(Token::COLON_TOKEN);
                    break;
                    case '(' :
                        return Token(Token::LEFTPAREN_TOKEN);
                    break;
                    case ')' :
                        return Token(Token::RIGHTPAREN_TOKEN);
                    break;
                    case '{' :
                        return Token(Token::LEFTBRACE_TOKEN);
                    break;
                    case '}' :
                        return Token(Token::RIGHTBRACE_TOKEN);
                    break;
                    case '!' :
                        if(reader.nextChar() == '=')
                        {
                            return Token(Token::NOTEQUAL_TOKEN);
                        }
                        else
                        {
                            reader.prevChar();
                            return Token(Token::NOT_TOKEN);
                        }
                    break;
                    case '+' :
                        if(reader.nextChar() == '=')
                        {
                            return Token(Token::PLUSASSIGN_TOKEN);
                        }
                        else
                        {
                            reader.prevChar();
                        }
                        if(reader.nextChar() == '+')
                        {
                            return Token(Token::PLUSPLUS_TOKEN);
                        }
                        else
                        {
                            reader.prevChar();
                        }
                        return Token(Token::PLUS_TOKEN);
                    break;
                    case '-' :
                        if(reader.nextChar() == '=')
                        {
                            return Token(Token::MINUSASSIGN_TOKEN);
                        }
                        else
                        {
                            reader.prevChar();
                        }
                        if(reader.nextChar() == '-')
                        {
                            return Token(Token::MINUSMINUS_TOKEN);
                        }
                        else
                        {
                            reader.prevChar();
                        }
                        return Token(Token::MINUS_TOKEN);
                    break;
                    case '*' :
                        return Token(Token::MULT_TOKEN);
                    break;
                    case '=' :
                        if(reader.nextChar() == '=')
                        {
                            return Token(Token::EQUAL_TOKEN);
                        }
                        else
                        {
                            reader.prevChar();
                            return Token(Token::ASSIGN_TOKEN);
                        }
                    case '>' :
                        if(reader.nextChar() == '=')
                        {
                            return Token(Token::GREATEREQUAL_TOKEN);
                        }
                        else
                        {
                            reader.prevChar();
                            return Token(Token::GREATER_TOKEN);
                        }
                    break;
                    case '<' :
                        if(reader.nextChar() == '=')
                        {
                            return Token(Token::LESSEQUAL_TOKEN);
                        }
                        else
                        {
                            reader.prevChar();
                            return Token(Token::LESS_TOKEN);
                        }
                    case '/':
                        state = SLASH_STATE;
                    break;
                    case '&':
                        if(reader.nextChar() == '&')
                        {
                            return Token(Token::AND_TOKEN);
                        }
                        else
                        {
                            reader.prevChar();
                            errors.push_back(ScannerError(ScannerError::SYNTAX_ERROR,"Only one &",line));
                        }
                    break;
                    case '|':
                        if(reader.nextChar() == '|')
                        {
                            return Token(Token::OR_TOKEN);
                        }
                        else
                        {
                            reader.prevChar();
                            errors.push_back(ScannerError(ScannerError::SYNTAX_ERROR,"Only one |",line));
                        }
                    break;
                    case '\n': case '\r':
                        reader.prevChar();
                        matchLineEnd();
                    break;
                    case 'a': case 'b': case 'c': case 'd': case 'e': 
                    case 'f': case 'g': case 'h': case 'i': case 'j': 
                    case 'k': case 'l': case 'm': case 'n': case 'o': 
                    case 'p': case 'q': case 'r': case 's': case 't': 
                    case 'u': case 'v': case 'w': case 'x': case 'y': 
                    case 'z': case 'A': case 'B': case 'C': case 'D': 
                    case 'E': case 'F': case 'G': case 'H': case 'I': 
                    case 'J': case 'K': case 'L': case 'M': case 'N': 
                    case 'O': case 'P': case 'Q': case 'R': case 'S': 
                    case 'T': case 'U': case 'V': case 'W': case 'X': 
                    case 'Y': case 'Z':
                        state = IDENTIFIER_STATE;
                        reader.prevChar();
                    break;
                    case '1': case '2': case '3': case '4': case '5': 
                    case '6': case '7': case '8': case '9': case '0':
                        state = INTEGER_STATE;
                        reader.prevChar();
                    break;
                }
            break;
            case SLASH_STATE:
                switch(reader.nextChar())
                {
                    case '/':
                        strbuf.clear();
                        charbuf = reader.nextChar();
                        while(charbuf != '\n' && charbuf != '\r')
                        {
                            strbuf.push_back(charbuf);
                            charbuf = reader.nextChar();
                        }
                        reader.prevChar();
                        state = START_STATE;
                        return Token(Token::LINECOMMENT_TOKEN,strbuf);
                    break;
                    case '*':
                        strbuf.clear();
                        boolbuf = false;
                        while(!boolbuf)
                        {
                            charbuf = reader.nextChar();
                            if(charbuf != '\0')
                            {
                                if(charbuf == '\r' || charbuf == '\n')
                                {
                                    reader.prevChar();
                                    matchLineEnd();
                                }
                                if(charbuf == '*')
                                {
                                    charbuff = reader.nextChar();
                                    if(charbuff == '/')
                                    {
                                        boolbuf = true;
                                    }
                                    else
                                    {
                                        strbuf.push_back(charbuf);
                                        strbuf.push_back(charbuff);
                                    }
                                }
                                else
                                {
                                    strbuf.push_back(charbuf);
                                }
                            }
                            else
                            {
                                boolbuf = true;
                            }
                        }
                        state = START_STATE;
                    break;
                    default:
                        reader.prevChar();
                        state = START_STATE;
                        return Token(Token::DIV_TOKEN);
                    break;
                }
            break;
            case IDENTIFIER_STATE:
                strbuf.clear();
                charbuf = reader.nextChar();
                while((charbuf >= 'a' && charbuf <= 'z') || (charbuf >= 'A' && charbuf <= 'Z'))
                {
                    strbuf.push_back(charbuf);
                    charbuf = reader.nextChar();
                }
                reader.prevChar();
                state = START_STATE;
                if(strbuf == "var")
                {
                    return Token(Token::VAR_TOKEN);
                }
                else if(strbuf == "int" || strbuf == "bool")
                {
                    return Token(Token::TYPE_TOKEN,strbuf);
                }
                else if(strbuf == "true" || strbuf == "false")
                {
                    return Token(Token::BOOL_TOKEN,strbuf);
                }
                else if(strbuf == "if")
                {
                    return Token(Token::IF_TOKEN);
                }
                else if(strbuf == "else")
                {
                    return Token(Token::ELSE_TOKEN);
                }
                else if(strbuf == "while")
                {
                    return Token(Token::WHILE_TOKEN);
                }
                else if(strbuf == "print")
                {
                    return Token(Token::PRINT_TOKEN);
                }
                else
                {
                    return Token(Token::IDENTIFIER_TOKEN,strbuf);
                }
            break;
            case INTEGER_STATE:
                strbuf.clear();
                charbuf = reader.nextChar();
                while((charbuf >= '0' && charbuf <= '9'))
                {
                    strbuf.push_back(charbuf);
                    charbuf = reader.nextChar();
                }
                reader.prevChar();
                state = START_STATE;
                return Token(Token::INTEGER_TOKEN,strbuf);
            break;
        }
    }
}
int Scanner::getLine()
{
    return line;
}
