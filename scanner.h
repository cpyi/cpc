#include <string>
#include <vector>
#include <iostream>
#include "reader.h"
#ifndef _SCANNER_H_
#define _SCANNER_H_
class Token
{
    public:
        enum TokenType
        {
            EOF_TOKEN,COLON_TOKEN,SEMICOLON_TOKEN,LEFTPAREN_TOKEN,
            RIGHTPAREN_TOKEN,LEFTBRACE_TOKEN,RIGHTBRACE_TOKEN,
            IDENTIFIER_TOKEN,PLUS_TOKEN,PLUSPLUS_TOKEN,PLUSASSIGN_TOKEN,
            MINUS_TOKEN,MINUSMINUS_TOKEN,MINUSASSIGN_TOKEN,MULT_TOKEN,
            DIV_TOKEN,ASSIGN_TOKEN,EQUAL_TOKEN,NOTEQUAL_TOKEN,
            GREATER_TOKEN,GREATEREQUAL_TOKEN,LESS_TOKEN,LESSEQUAL_TOKEN,
            AND_TOKEN,OR_TOKEN,NOT_TOKEN,LINECOMMENT_TOKEN,BLOCKCOMMENT_TOKEN,
            TYPE_TOKEN,BOOL_TOKEN,INTEGER_TOKEN,VAR_TOKEN,IF_TOKEN,ELSE_TOKEN,
            WHILE_TOKEN,PRINT_TOKEN 
        };
        Token(){}
        Token(TokenType tt)
            :token_type(tt),token_context(""){}
        Token(TokenType tt,std::string tc)
            :token_type(tt),token_context(tc){}
        void dump();
        TokenType getType();
        std::string getContext();
    private:
        TokenType token_type;
        std::string token_context;
};
class ScannerError
{
    public:
        enum ScannerErrorType
        {
            SYNTAX_ERROR
        };
        ScannerError(ScannerErrorType type,std::string msg,int line)
            :type(type),msg(msg),line(line){}
    private:
        ScannerErrorType type;
        std::string msg;
        int line;
};
class Scanner
{
    public:
        Scanner(Reader reader):reader(reader),state(START_STATE),line(0){}
        Token nextToken();
        int getLine();
        void matchLineEnd();
        enum ScannerState
        {
            START_STATE,
            SLASH_STATE,
            INTEGER_STATE,
            IDENTIFIER_STATE
        };
    private:
        Reader reader;
        ScannerState state;
        std::string strbuf;
        char charbuf;
        char charbuff;
        bool boolbuf;
        int line;
        std::vector<ScannerError> errors;
};
#endif
